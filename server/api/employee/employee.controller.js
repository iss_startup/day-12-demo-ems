/**
 * Created by vishnu on 30/10/16.
 */
var Employees=require('../../database').Employees;
var Salaries=require('../../database').Salary;

module.exports={
    list : function (req, res) {

        var employeeName = req.query.emp_name;
        var employeeDBName = req.query.db_name;
        var whereObj = {}
        whereObj[employeeDBName] = employeeName;
        console.log('-----------', whereObj);
        Employees
            .findAll({
                where: whereObj,
                limit: 10
            })
            .then(function (result) {

                if (result) {
                    res.json(result);
                } else {
                    res.status(400).send(JSON.stringify("Record Not Found"));
                }
            });

    },
    get : function (req, res) {

        var employeeNo = req.query.emp_no;

        Employees
            .find({
                where: {
                    emp_no: employeeNo
                },
                order: [
                    ['emp_no', 'ASC'],
                    [Salaries, 'from_date', 'DESC']
                ],
                limit: 10,
                include: [
                    Salaries
                ]
            })
            .then(function (result) {
                if (result && result.dataValues) {
                    res.json(result.dataValues);
                } else {
                    res.status(400).send(JSON.stringify("Record Not Found"));
                }
            });

    },
    update : function (req, res) {
        console.log(req.body);

        Employees.find({where: {emp_no: req.body.emp_no}}).then(function (result) {
            if (result) {
                result.updateAttributes(
                    {
                        first_name: req.body.emp_name
                    }
                ).then(function (data1) {
                    console.log(data1);
                })
            }

        });
    },
    
    create : function (req, res) {
        console.log("hihi");


        //var emp_no = genereateEmpNo(); //alter emp_no to be AUTOINCREMENT for now.

        var gender = req.body.gender == "male" ? 'M' : 'F'; // enum value in the table
        var birth = req.body.birthday.substring(0, req.body.birthday.indexOf('T')); // format date
        var hire = req.body.hiredate.substring(0, req.body.hiredate.indexOf('T')); // format date
        var lastname = req.body.lastname;
        var firstname = req.body.firstname;
        var emp_no = req.body.empNo;


        Employees.create({
            emp_no: emp_no,
            birth_date: birth,
            first_name: firstname,
            last_name: lastname,
            gender: gender,
            hire_date: hire

        }).then(function (result) {
            console.log('--------------', result);
            if (result && result.dataValues) {
                res.json(result.dataValues);
            } else {
                console.log('-------------------')
                throw new Error({error:[{message:'Record ready in use!'}]});
                res.status(400).send(JSON.stringify("Record Not Found"));
            }

        }).catch(function (error) {
            console.log('---------error------------',error)
            res.status(400).send({error : "Record Already in Use"});
        })

    }
    
}